#version 330

uniform sampler2D uSamplerColor;
uniform sampler2D uSamplerNormal;

uniform sampler2D uSamplerColor2;
uniform sampler2D uSamplerNormal2;

in vec2 vTexCoord;

out vec4 oColor;

void main()
{
    vec4 color  = texture(uSamplerColor, vTexCoord);
    vec4 normal = texture(uSamplerNormal, vTexCoord);

    vec2 pos = vTexCoord;

    if (pos.y < 0.5) {
        color  = texture(uSamplerColor2, vTexCoord);
        normal = texture(uSamplerNormal2, vTexCoord);
    }

    if(pos.x < 0.5)
    {
        oColor = color;
    }
    else
    {
        oColor = normal;
    }
}
