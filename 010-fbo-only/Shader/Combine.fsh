#version 150

uniform sampler2D uSamplerColor;
uniform sampler2D uSamplerNormal;

in vec2 vTexCoord;

out vec4 oColor;

void main()
{
    vec4 color  = texture(uSamplerColor, vTexCoord);
    vec4 normal = texture(uSamplerNormal, vTexCoord);

    vec2 pos = vTexCoord;

    if (pos.x < 0.5)
    {
        oColor = color;
    }
    else
    {
        oColor = normal;
    }
}
