#include "../shared/main.hh"

#include <ACGL/OpenGL/Objects.hh>
#include <ACGL/Base/Settings.hh>
#include <ACGL/Math/Math.hh>
#include <ACGL/OpenGL/Creator/ShaderProgramCreator.hh>
#include <ACGL/OpenGL/Creator/VertexArrayObjectCreator.hh>

using namespace ACGL::OpenGL;
using namespace ACGL::Base;
using namespace ACGL::Utils;

SharedVertexArrayObject vaoBunny;
SharedShaderProgram basicInformation;
SharedShaderProgram combine;
SharedFrameBufferObject fbo;
std::vector<SharedTexture2D> offScreenTextures;

glm::uvec2 bunnyRes;

//
// NOTE: this is a simplified version of 009-ubo-fbo that only needs FBOs for testing
//

// gets called after the OpenGL window is prepared:
void initCustomResources()
{
    Settings::the()->setResourcePath("../");
    Settings::the()->setShaderPath("010-fbo-only/Shader/");
    Settings::the()->setGeometryPath("shared/Geometry/");

    bunnyRes = g_windowSize / (unsigned int) 4;

    // store the offscreen textures so they can be resized if the window resizes:
    offScreenTextures.push_back( SharedTexture2D( new Texture2D(bunnyRes) ) ); // RGBA per default
    offScreenTextures.push_back( SharedTexture2D( new Texture2D(bunnyRes) ) ); // RGBA per default
    offScreenTextures.push_back( SharedTexture2D( new Texture2D(bunnyRes, GL_DEPTH24_STENCIL8) ) );

    offScreenTextures[0]->setMinFilter( GL_NEAREST );
    offScreenTextures[1]->setMinFilter( GL_NEAREST );
    offScreenTextures[0]->setMagFilter( GL_NEAREST );
    offScreenTextures[1]->setMagFilter( GL_NEAREST );

    // set up the FBO:
    fbo = SharedFrameBufferObject(new FrameBufferObject());
    fbo->attachColorTexture( "oNormal", offScreenTextures[1] );
    fbo->attachColorTexture( "oColor",  offScreenTextures[0] );
    fbo->setDepthTexture(               offScreenTextures[2] );
    fbo->validate(); // always a good idea

    // shader for the first render pass:
    basicInformation = ShaderProgramCreator("BasicInformation") // load all shader files starting with BasicInformation
            .fragmentDataLocations( fbo->getAttachmentLocations() )  // use the fragdata locations from the FBO
            .uniformBufferLocation( "bGlobalMatrices", 0 )           // set a location for the uniform buffer
            .create();

    // shader for the second render pass:
    combine = ShaderProgramCreator("Combine").create();

    // load the geometry of the stanford bunny and build a VAO:
    vaoBunny = VertexArrayObjectCreator("Bunny.obj").create();
    vaoBunny->setAttributeLocations( basicInformation->getAttributeLocations() );
}

void deleteCustomResources() {}

void draw( float runTime )
{
    //
    // first render pass
    fbo->bind();
    glEnable(GL_DEPTH_TEST);
    glViewport( 0, 0, bunnyRes.x, bunnyRes.y );
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    basicInformation->use();
    glm::mat4 viewMatrix = glm::translate(glm::vec3(0.0f, -1.0f, -2.0f)) * glm::rotate<float>(1.0472f * runTime, glm::vec3(0.0f, 1.0f, 0.0f)) * glm::scale<float>(glm::vec3(0.25f));
    basicInformation->setUniform( "uViewMatrix", viewMatrix );
    basicInformation->setUniform( "uProjectionMatrix", buildFrustum(75.0, 0.1, 100.0, (float)bunnyRes.x/(float)bunnyRes.y) );

    vaoBunny->render();

    //
    // second render pass
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glDisable(GL_DEPTH_TEST);
    glViewport( 0, 0, g_windowSize.x, g_windowSize.y );

    combine->use();
    combine->setTexture("uSamplerColor",  offScreenTextures[0], 0 );
    combine->setTexture("uSamplerNormal" ,offScreenTextures[1], 1 );

    // attribute-less rendering:
    VertexArrayObject vao;
    vao.bind(); // 'empty' VAO -> no attributes are defined
    glDrawArrays( GL_TRIANGLE_STRIP, 0, 4 ); // create 2 triangles with no attributes
}

void resizeCallback( GLFWwindow *, int newWidth, int newHeight )
{
    g_windowSize = glm::uvec2( newWidth, newHeight);
    bunnyRes = g_windowSize / (unsigned int) 4; // offscreen pass should be in a lower resolution than the onscreen pass

    // resize offscreen textures:
    for ( auto tex : offScreenTextures ) {
        tex->resize( bunnyRes );
    }
}

