#!/bin/bash

#
# A specific compiler can be given as a commandline argument, the compilername has to be supported by the CMakeLists
# file of the examples (depending on the compiler other flags have to be set)
#
# See SharedCMakeLists.txt for supported strings.
#
if [ $# -eq 1 ] ; then
    USECOMPILER=$1
else
    USECOMPILER=default
fi


#
# Test if glfw was build, if not, quickstart.sh was not run.
# 
function runQuickstartFor {
  	if [ ! -f $1 ];
	then
		echo "GLFW lib not found ( "$1" ) - trying to build it..."
		./quickstart.sh
	fi

	if [ ! -f $1 ];
	then
		echo "GLFW lib not found ( "$1" ) - check why quickstart.sh did not work!"
		exit -1
	fi
}

os=`uname`
if [[ "$os" == 'Linux' ]]; then
	runQuickstartFor ./extern/glfw/src/libglfw3.a
elif [[ "$os" == 'Darwin' ]]; then
	runQuickstartFor ./extern/glfw/src/libglfw3.a
else
   echo "panic: OS not detected - FIX ME"
fi

echo "start build"


function mkbuilddir {
    if [ ! -d ./build ];
    then
        mkdir build
    fi
    if [ ! -d ./build/$1 ];
    then
        mkdir build/$1
    fi
}

function buildExample {
    echo -n "building example "
    echo $1
    echo
    mkbuilddir $1
    cd build/$1
    cmake -DUSE_COMPILER=$USECOMPILER ../../$1 &&
    make clean
    make -j8
    cd ../..
    echo
    echo "done"
    echo
}

#
# Iterate over all directories, that have a trailing 0 and a '-' as the third char.
# You will have to fix this script for example 100...
#
for directory in 0??-*
do
    buildExample $directory
done

echo
echo "all done"
echo "NOTE: you must run the examples from within the binaries directory - otherwise they won't find there files"
echo
