#version 150

void main()
{
    float id = float(gl_VertexID);

    id = (2.0*3.141592*id) / 60.0;

    float f = float(gl_InstanceID);
    f /= 100.0;

    gl_Position = vec4( f*0.9*(3.0/4.0)*sin(id), f*0.9*cos(id), -0.5, 1);
}
