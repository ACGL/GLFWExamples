#include "../shared/main.hh"

#include <ACGL/OpenGL/Controller.hh>
#include <ACGL/OpenGL/Objects.hh>
#include <ACGL/Base/Settings.hh>
#include <ACGL/Math/Math.hh>

using namespace std;
using namespace ACGL::OpenGL;
using namespace ACGL::Base;
using namespace ACGL::Utils;

SharedVertexArrayObject emptyVAO;
SharedShaderProgram normalsAsColorShader;

// gets called after the OpenGL window is prepared:
void initCustomResources()
{
    // define where shaders and geometry can be found:
    Settings::the()->setResourcePath("../");
    Settings::the()->setShaderPath("013-attributeless-rendering/Shader/");

    // look up all shader files starting with 'HelloWorld' and build a ShaderProgram from it:
    normalsAsColorShader = ShaderProgramControlFiles("attributeless-rendering").create();
    normalsAsColorShader->use();

    // rendering with VAO 0 is illegal in core:
    emptyVAO = SharedVertexArrayObject( new VertexArrayObject() );
    emptyVAO->bind();

    glPointSize( 3.0 );

    // just in case: check for errors
    openGLCriticalError();
}

void deleteCustomResources()
{
    // we have memory management via reference counting, so nothing to do here
}

void draw( float runTime )
{
    // clear the framebuffer:
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    int x = fmod(runTime, 1.0f)*60.0f;
    glDrawArraysInstanced( GL_POINTS, x, 1,  80 );

    x = fmod(runTime/60.0f, 1.0f)*60.0f;
    glDrawArraysInstanced( GL_POINTS, x, 1, 100 );
}

void resizeCallback( GLFWwindow *, int newWidth, int newHeight )
{
    // store the new window size and adjust the viewport:
    g_windowSize = glm::uvec2( newWidth, newHeight);
    glViewport( 0, 0, g_windowSize.x, g_windowSize.y );
}

