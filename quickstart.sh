#!/bin/bash

# 1) Build glfw (on non-unix system, you will need other parameters and you might adapt the cmake files of the examples)
#os=`uname`
#if [[ "$os" == 'Linux' ]]; then
#   echo "building glfw for Linux..."
#   cd extern/glfw/ && make -j x11 ; cd ../..
#elif [[ "$os" == 'Darwin' ]]; then
#   echo "building glfw for MacOS..."
#   cd extern/glfw/ && make -j cocoa ; cd ../..
#else
#   echo "panic: OS not detected - FIX ME"
#fi

cd extern/glfw
cmake CMakeLists.txt
make
cd ../..


echo
echo "Open the CMakeFile.txt of an example, e.g. 001-hello-world/CMakeLists.txt in QTCreator (or any other IDE)"
echo "> qtcreator 001-hello-world/CMakeLists.txt"
echo
