#include <ACGL/OpenGL/GL.hh> // this has to be included before glfw.h !
#include <GLFW/glfw3.h>


#include <ACGL/OpenGL/Objects.hh>
#include <ACGL/OpenGL/Data/TextureData.hh>
#include <ACGL/OpenGL/Data/TextureLoadStore.hh>
#include <ACGL/Math/Math.hh>
#include <ACGL/Utils/FileHelpers.hh>
#include <ACGL/OpenGL/Creator/VertexArrayObjectCreator.hh>
#include <ACGL/OpenGL/Creator/ShaderProgramCreator.hh>
#include <ACGL/OpenGL/Managers.hh>
#include <ACGL/Scene/GenericCamera.hh>
#include <ACGL/Base/Settings.hh>


using namespace std;
using namespace ACGL::OpenGL;
using namespace ACGL::Base;
using namespace ACGL::Utils;
using namespace ACGL::Scene;

// global variables are used to keep the example small
//
// the size of the window to open:
glm::uvec2 g_windowSize( 640, 480 );
bool glfwWindowClosed = false;
GenericCamera g_camera;
SharedArrayBuffer bunny;
SharedVertexArrayObject vaoBunny;
ConstSharedShaderProgram simpleShader;
SharedTexture2D bunnyTexture;

GLFWwindow* g_window;

void setGLFWHintsForOpenGLVersion( unsigned int _version )
{
#ifdef __APPLE__
#if (ACGL_OPENGL_VERSION >= 30)
    // request OpenGL 3.2, will return a 4.1 context on Mavericks
    glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 3 );
    glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 2 );
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
#else
// non-apple
    glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, _version / 10 );
    glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, _version % 10 );
    #ifdef ACGL_OPENGL_PROFILE_CORE
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    #endif
#endif
}


/**********************************************************************************************************************
 * Returns true if a window with the desired context could get created.
 * Requested OpenGL version gets set by ACGL defines.
 */
bool createWindow()
{
    /////////////////////////////////////////////////////////////////////////////////////
    // Initialise GLFW
    //
    if ( !glfwInit() )
    {
        error() << "Failed to initialize GLFW" << endl;
        exit( -1 );
    }

    /////////////////////////////////////////////////////////////////////////////////////
    // Configure OpenGL context
    //
    setGLFWHintsForOpenGLVersion( ACGL_OPENGL_VERSION );

    /////////////////////////////////////////////////////////////////////////////////////
    // try to create an OpenGL context in a window and check the supported OpenGL version:
    //
    //                                                    R,G,B,A, Depth,Stencil
    g_window = glfwCreateWindow( g_windowSize.x, g_windowSize.y, "ACGL GLFWExamples", NULL, NULL);
    if (!g_window) {
        error() << "Failed to open a GLFW window - requested OpenGL: " <<  ACGL_OPENGL_VERSION << endl;
        return false;
    }
    glfwMakeContextCurrent(g_window);
    ACGL::init();

    return true;
}

void initResources()
{
    // define where shaders can be found:
    Settings::the()->setShaderPath("shader/");

    bunnyTexture = loadTexture2D( "../../shared/Geometry/clownfishBunny.png" );

    // look up all shader files starting with 'HelloWorld' and build a ShaderProgram from it:
    // store it in a manager to allow semi-automatic reloading
    simpleShader = ShaderProgramFileManager::the()->get( ShaderProgramCreator("HelloWorld") );
    simpleShader->use();
    simpleShader->setTexture( "uTexture", bunnyTexture, 0 );

    // load the geometry of the stanford bunny and build a VAO:
    vaoBunny = VertexArrayObjectCreator("../../shared/Geometry/Bunny.obj").create();

    // connect the attribute locations as they are defined in the shader to the bunny:
    vaoBunny->setAttributeLocations( simpleShader->getAttributeLocations() );

    // the other way round is also possible: set the shader according to the mesh:
    //normalsAsColorShader->setAttributeLocations( vaoBunny->getAttributeLocations() );

    // just in case: check for errors
    openGLCriticalError();
}

void draw( double time )
{
    static double nextReloadTime = 1.0;
    if (time > nextReloadTime) {
        ShaderProgramFileManager::the()->updateAll();
        nextReloadTime = time + 1.0; // check again in one second
    }

    // clear the framebuffer:
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // set view and projection matrix:
    simpleShader->use();
    simpleShader->setUniform( "uViewMatrix", g_camera.getViewMatrix() );
    simpleShader->setUniform( "uProjectionMatrix", g_camera.getProjectionMatrix() );
    simpleShader->setUniform( "uNormalMatrix", g_camera.getRotationMatrix3() ); // assuming just a rotation, the rotation matrix is the normal matrix
    openGLCriticalError();

    // render the bunny:
    vaoBunny->bind();
    vaoBunny->render();
    openGLCriticalError();
}

void handleInput( double time )
{
    static double timeOfLastFrame = 0.0;
    static bool spaceIsPressed = false;
    if (timeOfLastFrame == 0.0) {
        timeOfLastFrame = time;
        // ignore the first frame, as the user has not done useful inputs till now anyway
        return;
    }

    // make camera movements based on the elapsed time and not based on frames rendered!
    double timeElapsed = time - timeOfLastFrame;

    double speed = 5.0; // magic value to scale the camera speed

    // as long as the keys are hold down, these are triggered each frame:
    if (glfwGetKey( g_window, 'W')) { // upper case!
        g_camera.moveForward( timeElapsed*speed );
    }
    if (glfwGetKey( g_window, 'A')) { // upper case!
        g_camera.moveLeft( timeElapsed*speed );
    }
    if (glfwGetKey( g_window, 'S')) { // upper case!
        g_camera.moveBack( timeElapsed*speed );
    }
    if (glfwGetKey( g_window, 'D')) { // upper case!
        g_camera.moveRight( timeElapsed*speed );
    }

    // only react on pressed and releases. There are other ways, look up the GLFW documentation
    if (!spaceIsPressed && glfwGetKey( g_window, GLFW_KEY_SPACE)) {
        // space is just pressed down
        debug() << "space pressed" << endl;
        spaceIsPressed = true;
    } else if (spaceIsPressed && !glfwGetKey( g_window, GLFW_KEY_SPACE)) {
        debug() << "space released" << endl;
        spaceIsPressed = false;
    }
    // window can be closed by pressing ESC
    if ( glfwGetKey( g_window, GLFW_KEY_ESCAPE) ) {
        glfwSetWindowShouldClose( g_window, GL_TRUE );
    }


    timeOfLastFrame = time;
}

void mouseMoveCallback( GLFWwindow *, double x, double y )
{
    static glm::ivec2 initialPosition;
    static bool leftMouseButtonDown = false;

    if (!leftMouseButtonDown && glfwGetMouseButton( g_window, GLFW_MOUSE_BUTTON_1 )) {
        leftMouseButtonDown = true;
        initialPosition = glm::ivec2( x,y );
        glfwSetInputMode( g_window, GLFW_CURSOR_HIDDEN, true ); // hide the cursor
        return;
    } else if (leftMouseButtonDown && !glfwGetMouseButton( g_window, GLFW_MOUSE_BUTTON_1 )) {
        leftMouseButtonDown = false;
        glfwSetInputMode( g_window, GLFW_CURSOR_HIDDEN, false ); // unhide the cursor
        glfwSetCursorPos( g_window, initialPosition.x, initialPosition.y );
        return;
    }

    glm::ivec2 movement = glm::ivec2( x,y ) - initialPosition;

    if (leftMouseButtonDown) {
        glm::vec2 realtiveMovement = glm::vec2(movement) / glm::vec2(g_windowSize);
        g_camera.FPSstyleLookAround( realtiveMovement.x, realtiveMovement.y );
        glfwSetCursorPos( g_window, initialPosition.x, initialPosition.y );
    }
}

void resizeCallback( GLFWwindow *, int newWidth, int newHeight )
{
    // store the new window size and adjust the viewport:
    g_windowSize = glm::uvec2( newWidth, newHeight );
    glViewport( 0, 0, g_windowSize.x, g_windowSize.y );

    // update the camera:
    g_camera.resize( newWidth, newHeight );
}

int main( int argc, char *argv[] )
{
    /////////////////////////////////////////////////////////////////////////////////////
    // Create OpenGL capable window:
    //
    if ( !createWindow() ) {
        glfwTerminate();
        exit( -1 );
    }

    /////////////////////////////////////////////////////////////////////////////////////
    // Set window title to binary name (without the path):
    //
    std::vector<std::string> tmp = StringHelpers::split( std::string( argv[0] ), '/' );
    glfwSetWindowTitle( g_window, tmp[tmp.size()-1].c_str() );

    glfwSetWindowSizeCallback(  g_window, resizeCallback );

    // Enable vertical sync (on cards that support it) with parameter 1 - 0 means off
    glfwSwapInterval( 1 );

    /////////////////////////////////////////////////////////////////////////////////////
    // OpenGL state:
    //
    glClearColor( 0.0, 0.0, 0.0, 1.0 );
    glEnable( GL_DEPTH_TEST );


    //
    // configure input:
    //
    // don't miss short keypresses, which are shorter than one frame and would be missed otherwise:
    glfwSetInputMode( g_window, GLFW_STICKY_KEYS, true );
    //
    // handle the mouse as a callback:
    glfwSetCursorPosCallback( g_window, mouseMoveCallback );


    initResources();
    double startTimeInSeconds = glfwGetTime();

    do {
        double now = glfwGetTime() - startTimeInSeconds;

        glfwPollEvents();
        handleInput( now );
        draw( now );

        glfwSwapBuffers( g_window );
        openGLCriticalError();
    } while( !glfwWindowShouldClose( g_window ) );

    glfwTerminate();
    exit(0);
}

