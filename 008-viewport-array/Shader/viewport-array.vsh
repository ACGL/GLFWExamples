#version 330

in vec3 aNormal;
in vec3 aPosition;
in vec2 aTexCoord;

out vec3 vNormal;
out vec2 vTexCoord;

void main()
{
    // everything gets passed tru as the GS will do the transformations
    vNormal     = aNormal;
    vTexCoord   = aTexCoord;
    gl_Position = vec4(aPosition, 1.0);
}
