#version 330

in vec3 gNormal;
in vec2 gTexCoord;

out vec4 oColor;

uniform sampler2D uTexture;

void main()
{
    vec3 color = texture(uTexture, gTexCoord).rgb;
    color *= dot(normalize(gNormal),vec3(0,0,1));

    oColor = vec4( color, 1.0 );
}
