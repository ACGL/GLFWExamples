#version 410

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;
layout(triangles, invocations = 4) in; // call the GS 4 times to generate 4 versions

// two viewmatrices to create different transformations for different viewports
uniform mat4 uViewMatrix1;
uniform mat4 uViewMatrix2;
uniform mat4 uProjectionMatrix;

in vec3 vNormal[];
in vec2 vTexCoord[];

out vec3 gNormal;
out vec2 gTexCoord;

// 08/15 projection:
void fullBunny(in mat4 viewMatrix) {
    for (int i = 0; i < 3; i++)
    {
        gNormal     = inverse(transpose(mat3(viewMatrix))) * vNormal[i];
        gTexCoord   = vTexCoord[i];
        gl_Position = uProjectionMatrix * viewMatrix * gl_in[i].gl_Position;

        EmitVertex();
    }
    EndPrimitive();
}

// set the positions to the texture coordinates to look at the texture map (but with realistic lighting)
void textureMap(in mat4 viewMatrix) {
    for (int i = 0; i < 3; i++)
    {
        gNormal     = inverse(transpose(mat3(viewMatrix))) * vNormal[i];
        gTexCoord   = vTexCoord[i];

        vec2 pos = vTexCoord[i].st;
        pos -= 0.5;
        pos *= 2.0;
        gl_Position = vec4( pos, 0.5, 1.0);

        EmitVertex();
    }
    EndPrimitive();
}

void main()
{
    // defines the viewport to use:
    gl_ViewportIndex = gl_InvocationID;

    // depending on the invocation id (0..3 as this shader demands 4 invocations) different transformations will get performed:
    if (gl_InvocationID == 0) {
        textureMap(uViewMatrix1);
    } else if (gl_InvocationID == 1) {
        fullBunny(uViewMatrix1);
    } else if (gl_InvocationID == 2) {
        textureMap(uViewMatrix2);
    } else {
        fullBunny(uViewMatrix2);
    }
}
