#include "../shared/main.hh"

#include <ACGL/OpenGL/Managers.hh>
#include <ACGL/OpenGL/Creator/ShaderProgramCreator.hh>
#include <ACGL/OpenGL/Creator/VertexArrayObjectCreator.hh>
#include <ACGL/OpenGL/Objects.hh>
#include <ACGL/Base/Settings.hh>
#include <ACGL/Math/Math.hh>
#include <ACGL/OpenGL/Data/TextureDataLoadStore.hh>

#include <fstream>

using namespace std;
using namespace ACGL::OpenGL;
using namespace ACGL::Base;
using namespace ACGL::Utils;

SharedVertexArrayObject vaoGeometry;
ConstSharedShaderProgram shaderProgram;
SharedTexture2D texture;

// Generates a .obj file with a random height field
void generateOBJ()
{
    const int width = 16;
    const int depth = 16;
    float y[width][depth];

    for(int x = 0; x < width; ++x)
    {
        for(int z = 0; z < depth; ++z)
        {
            y[x][z] = (float)rand() / RAND_MAX;
        }
    }

    std::ofstream outFileStream("../006-auto-reload/Generated/Geometry.obj");

    // write vertices
    for(int x = 0; x < width; ++x)
    {
        for(int z = 0; z < depth; ++z)
        {
            outFileStream << "v " << x - (width-1) / 2.0;
            outFileStream << " "  << y[x][z];
            outFileStream << " "  << z - (depth-1) / 2.0;
            outFileStream << std::endl;
        }
    }

    // write tex coords
    for(int x = 0; x < width; ++x)
    {
        for(int z = 0; z < depth; ++z)
        {
            outFileStream << "vt " << x / (float)(width-1);
            outFileStream << " "   << z / (float)(depth-1);
            outFileStream << std::endl;
        }
    }

    // write faces
    for(int x = 0; x < width-1; ++x)
    {
        for(int z = 0; z < depth-1; ++z)
        {
            outFileStream << "f " << 1 + (z+0)*width + x+0 << "/" << 1 + (z+0)*width + x+0;
            outFileStream << " "  << 1 + (z+0)*width + x+1 << "/" << 1 + (z+0)*width + x+1;
            outFileStream << " "  << 1 + (z+1)*width + x+0 << "/" << 1 + (z+1)*width + x+0;
            outFileStream << std::endl;

            outFileStream << "f " << 1 + (z+0)*width + x+1 << "/" << 1 + (z+0)*width + x+1;
            outFileStream << " "  << 1 + (z+1)*width + x+1 << "/" << 1 + (z+1)*width + x+1;
            outFileStream << " "  << 1 + (z+1)*width + x+0 << "/" << 1 + (z+1)*width + x+0;
            outFileStream << std::endl;
        }
    }

    outFileStream.close();
}

// Generates a .ppm file containing some noise
void generatePPM()
{
    std::ofstream outFileStream("../006-auto-reload/Generated/Texture.ppm");

    int width = 64;
    int height = 64;

    outFileStream << "P3" << std::endl;
    outFileStream << width << " " << height << std::endl;
    outFileStream << 255 << std::endl;

    float skew = -0.5 + rand() / (float)RAND_MAX;

    for(int x = 0; x < width; ++x)
    {
        for(int y = 0; y < height; ++y)
        {
            int grayValue = (int)(127 + 127*sin(x * 0.75 + y * skew));
            outFileStream << grayValue << " ";
            outFileStream << grayValue << " ";
            outFileStream << grayValue << std::endl;
        }
    }

    outFileStream.close();
}

// Generates a shader file that defines two functions returning constant colors
void generateShader()
{
    std::ofstream outFileStream("../006-auto-reload/Generated/Shader.fsh");

    outFileStream << "#version 150" << std::endl;

    outFileStream << "vec4 color1() { return vec4(";
    outFileStream << rand() / (float)RAND_MAX << ", ";
    outFileStream << rand() / (float)RAND_MAX << ", ";
    outFileStream << rand() / (float)RAND_MAX << ", ";
    outFileStream << "1.0); }" << std::endl;

    outFileStream << "vec4 color2() { return vec4(";
    outFileStream << rand() / (float)RAND_MAX << ", ";
    outFileStream << rand() / (float)RAND_MAX << ", ";
    outFileStream << rand() / (float)RAND_MAX << ", ";
    outFileStream << "1.0); }" << std::endl;

    outFileStream.close();
}

// gets called after the OpenGL window is prepared:
void initCustomResources()
{
    // initialize the randomly generated resource files:
    generateOBJ();
    generatePPM();
    generateShader();

    // define where shaders and geometry can be found:
    Settings::the()->setResourcePath("../");
    Settings::the()->setShaderPath("006-auto-reload/Shader/");
    Settings::the()->setGeometryPath("006-auto-reload/Generated/");

    // load the geometry and build a VAO:
    vaoGeometry = VAOFileManager::the()->get( VertexArrayObjectCreator("Geometry.obj") );

    // load a shader program that includes a generated shader file:
    shaderProgram = ShaderProgramFileManager::the()->get(ShaderProgramCreator("SimpleTexturing.vsh").
                                                     andFile("../Generated/Shader.fsh").
                                                     andFile("SimpleTexturing.fsh"));

    // the shaderProgram uses the generated texture:
    shaderProgram->use();

    generatePPM();
    SharedTextureData tData = loadTextureData( "../006-auto-reload/Generated/Texture.ppm" );
    texture = SharedTexture2D( new Texture2D() );
    texture->setImageData( tData );

    shaderProgram->setTexture( "uTexture", texture, 0 );

    // connect the attribute locations as they are defined in the shader to the VAO:
    vaoGeometry->setAttributeLocations( shaderProgram->getAttributeLocations() );

    // just in case: check for errors
    openGLCriticalError();

    vaoGeometry->bind();
}

void deleteCustomResources()
{
    // we have memory management via reference counting, so nothing to do here
}

float lastReload = 0.0f;

void draw( float runTime )
{
    // periodically generate new resource files:
    if(lastReload <= runTime - 2.0f)
    {
        generateOBJ();
        generateShader();
        lastReload = runTime;
    }

    // tell the file managers to reload their resources if the files have changed:
    ShaderProgramFileManager::the()->updateAll();
    VAOFileManager::the()->updateAll();

    // re-set the object states:
    shaderProgram->setTexture( "uTexture", texture, 0 );
    vaoGeometry->setAttributeLocations( shaderProgram->getAttributeLocations() );

    // clear the framebuffer:
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // set view and projection matrix:
    glm::mat4 viewMatrix = glm::translate(glm::vec3(0.0f, 0.0f, -2.0f))
                         * glm::rotate<float>(0.7854, glm::vec3(1.0f, 0.0f, 0.0f))  // 45 degree
                         * glm::rotate<float>(0.04363f * runTime, glm::vec3(0.0f, 1.0f, 0.0f)) // 2.5 degree
                         * glm::scale<float>(glm::vec3(0.25f));

    shaderProgram->use();
    shaderProgram->setUniform( "uViewMatrix", viewMatrix );
    shaderProgram->setUniform( "uProjectionMatrix", buildFrustum(75.0f, 0.1f, 100.0f, (float)g_windowSize.x/(float)g_windowSize.y) );

    // render the geometry:
    vaoGeometry->render();
}

void resizeCallback( GLFWwindow *, int newWidth, int newHeight )
{
    // store the new window size and adjust the viewport:
    g_windowSize = glm::uvec2( newWidth, newHeight);
    glViewport( 0, 0, g_windowSize.x, g_windowSize.y );
}

