#version 150

uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix;

in vec3 aPosition;
in vec2 aTexCoord;

out vec2 vTexCoord;

void main()
{
    vTexCoord = aTexCoord;
    gl_Position = uProjectionMatrix * uViewMatrix * vec4(aPosition, 1.0);
}
