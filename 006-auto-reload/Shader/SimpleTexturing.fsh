#version 150

uniform sampler2D uTexture;

in vec2 vTexCoord;

out vec4 oColor;

// Forward declaration for functions implemented in ../Generated/Shader.fsh
vec4 color1();
vec4 color2();

void main()
{
    float brightness = texture(uTexture, vTexCoord).r;
    oColor = mix(color1(), color2(), brightness);
}
