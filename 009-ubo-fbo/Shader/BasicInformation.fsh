#version 150

in vec3 vNormal;
in vec2 vTexCoord;

out vec4 oColor;
out vec4 oNormal;

void main()
{
    int pattern = ( int(gl_FragCoord.x) % 2) ^ ( int(gl_FragCoord.y) % 2);
    oColor = vec4(vTexCoord, 0.0, 1.0);
    oNormal = vec4(normalize(vNormal) * 0.5 + 0.5, 0.0);

    if (pattern == 0) {
        oColor  *= oColor;
        oNormal *= oNormal;
    }
}
