#version 150

layout(std140) uniform bGlobalMatrices
{
    mat4 uViewMatrix;
    mat4 uProjectionMatrix;
};

uniform mat4 uModelMatrix;

in vec3 aPosition;
in vec3 aNormal;
in vec2 aTexCoord;

out vec3 vNormal;
out vec2 vTexCoord;

void main()
{
    vNormal     = inverse(transpose(mat3(uViewMatrix))) * aNormal;
    vTexCoord   = aTexCoord;
    gl_Position = uProjectionMatrix * uViewMatrix * vec4(aPosition, 1.0);
}
