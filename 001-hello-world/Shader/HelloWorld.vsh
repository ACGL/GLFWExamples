#version 130

uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix;
uniform mat3 uNormalMatrix;

in vec3 aNormal;
in vec3 aPosition;
in vec2 aTexCoord;

out vec3 vNormal;

void main()
{
    vNormal     = uNormalMatrix * aNormal;
    gl_Position = uProjectionMatrix * uViewMatrix * vec4(aPosition, 1.0);
}
