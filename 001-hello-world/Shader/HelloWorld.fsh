#version 130

in vec3 vNormal;

out vec4 oColor;

void main()
{
    oColor = vec4(normalize(vNormal), 1.0);
}
