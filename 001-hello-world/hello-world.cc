#include "../shared/main.hh"

#include <ACGL/OpenGL/Creator/ShaderProgramCreator.hh>
#include <ACGL/OpenGL/Creator/VertexArrayObjectCreator.hh>
#include <ACGL/OpenGL/Objects.hh>
#include <ACGL/Base/Settings.hh>
#include <ACGL/Math/Math.hh>

using namespace std;
using namespace ACGL::OpenGL;
using namespace ACGL::Base;
using namespace ACGL::Utils;

SharedArrayBuffer bunny;
SharedVertexArrayObject vaoBunny;
SharedShaderProgram normalsAsColorShader;

// gets called after the OpenGL window is prepared:
void initCustomResources()
{
    // define where shaders can be found:
    Settings::the()->setShaderPath("../001-hello-world/Shader/");

    // look up all shader files starting with 'HelloWorld' and build a ShaderProgram from it:
    normalsAsColorShader = ShaderProgramCreator("HelloWorld").create();
    normalsAsColorShader->use();

    // load the geometry of the stanford bunny and build a VAO:
    vaoBunny = VertexArrayObjectCreator("../shared/Geometry/Bunny.obj").create();

    // connect the attribute locations as they are defined in the shader to the bunny:
    vaoBunny->setAttributeLocations( normalsAsColorShader->getAttributeLocations() );

    // the other way round is also possible: set the shader according to the mesh:
    //normalsAsColorShader->setAttributeLocations( vaoBunny->getAttributeLocations() );

    // just in case: check for errors
    openGLCriticalError();

    vaoBunny->bind();
}

void deleteCustomResources()
{
    // we have memory management via reference counting, so nothing to do here
}

void draw( float runTime )
{
    // clear the framebuffer:
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // set view and projection matrix:                                                          //60 degree to radians is 1,0472
    glm::mat4 viewMatrix = glm::translate(glm::vec3(0.0f, -1.0f, -2.0f)) * glm::rotate<float>(1.0472f * runTime, glm::vec3(0.0f, 1.0f, 0.0f)) * glm::scale<float>(glm::vec3(0.25f));
    normalsAsColorShader->setUniform( "uViewMatrix", viewMatrix );
    normalsAsColorShader->setUniform( "uProjectionMatrix", buildFrustum(75.0, 0.1, 100.0, (float)g_windowSize.x/(float)g_windowSize.y) );
    normalsAsColorShader->setUniform( "uNormalMatrix", glm::inverseTranspose( glm::mat3(viewMatrix) ) );
    openGLCriticalError();

    // render the bunny:
    vaoBunny->render();
    openGLCriticalError();
}

void resizeCallback( GLFWwindow *, int newWidth, int newHeight )
{
    // store the new window size and adjust the viewport:
    g_windowSize = glm::uvec2( newWidth, newHeight);
    glViewport( 0, 0, g_windowSize.x, g_windowSize.y );
}

