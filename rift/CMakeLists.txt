CMAKE_MINIMUM_REQUIRED (VERSION 2.6) 

# project/binary name:
PROJECT(rift_demo)

# ACGL setup:
SET(ACGL_OPENGL_SUPPORT CORE_32)
ADD_DEFINITIONS(-DACGL_OPENGL_VERSION_32)
ADD_DEFINITIONS(-DACGL_OPENGL_PROFILE_CORE)
ADD_DEFINITIONS(-DACGL_USE_OCULUS_RIFT)

###############################################################################
#
# Compiler settings, can be simpler if only one compiler should be used.
#



#Enable c++11
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
  #SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
  # using Visual Studio C++
endif()

# enable warnings
IF(WIN32)
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /MP /W3")
SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /MP /W3")
ELSE(WIN32)
ADD_DEFINITIONS(-Wall)
ENDIF(WIN32)
ADD_DEFINITIONS(-DNO_SPACE_NAVIGATOR_SUPPORT)

#
#
###############################################################################

###############################################################################
#
# Settings for the app.
#
#
# Lets the binary get written to a shared folder (which can be ignored by git).
# Will also set the run directory for QTCreator:
set(dir ${CMAKE_CURRENT_SOURCE_DIR}/bin)
set(EXECUTABLE_OUTPUT_PATH ${dir} CACHE PATH "Build directory" FORCE)

# source and header files
FILE(GLOB_RECURSE SOURCE_FILES "${CMAKE_SOURCE_DIR}/*.cc")
SET(SOURCE_FILES ${SOURCE_FILES} ${SOURCE_FILES_SHARED})

FILE(GLOB_RECURSE HEADER_FILES "${CMAKE_SOURCE_DIR}/*.hh")
SET(HEADER_FILES ${HEADER_FILES} ${HEADER_FILES_SHARED})

# shader files
FILE(GLOB_RECURSE SHADER_FILES "${CMAKE_SOURCE_DIR}/bin/shader/*.*")

#
#
###############################################################################

###############################################################################
#
# all we need for ACGL:
#
ADD_DEFINITIONS(-DACGL_ERROR_LEVEL_EC3)
INCLUDE(${CMAKE_SOURCE_DIR}/../extern/acgl/CMakeListsStaticInclude.txt)

#
# Oculus Rift:
#
INCLUDE(${CMAKE_SOURCE_DIR}/extern/LibOVR/CMakeListsStaticInclude.txt)

#
#
###############################################################################

###############################################################################
#
# GLFW (and some other linker flags)
#
FILE(GLOB_RECURSE HEADER_FILES_GLFW "${CMAKE_SOURCE_DIR}/../extern/glfw/include/*.h")
INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/../extern/glfw/include)
SET(HEADER_FILES ${HEADER_FILES} ${HEADER_FILES_GLFW})

add_subdirectory(../extern/glfw ${CMAKE_SOURCE_DIR}/../extern/glfw)

#
# Windows:
#
IF(WIN32)
  SET(LIBRARIES ${LIBRARIES}  glfw ${GLFW_LIBRARIES})
ENDIF(WIN32)

#
# MacOS X:
#
IF(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
  SET(LIBRARIES ${LIBRARIES} glfw ${GLFW_LIBRARIES} -Wl,-framework,Cocoa -Wl,-framework,OpenGL,-framework,IOKit)
ENDIF(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")

#
# Linux:
#
IF(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
  SET(LIBRARIES ${LIBRARIES} glfw ${GLFW_LIBRARIES} -lXrandr -lGL -lXi  -pthread -lm -lX11 -lXxf86vm)
ENDIF(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
#
#
#
###############################################################################

ADD_EXECUTABLE(${CMAKE_PROJECT_NAME} ${SOURCE_FILES} ${HEADER_FILES} ${SHADER_FILES} ${README_FILES})
TARGET_LINK_LIBRARIES(${CMAKE_PROJECT_NAME} ${LIBRARIES})
