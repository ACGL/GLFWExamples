#include <ACGL/OpenGL/GL.hh> // this has to be included before glfw.h !
#include <GLFW/glfw3.h>


#include <ACGL/OpenGL/Objects.hh>
#include <ACGL/OpenGL/Data/TextureData.hh>
#include <ACGL/OpenGL/Data/TextureLoadStore.hh>
#include <ACGL/Math/Math.hh>
#include <ACGL/Utils/FileHelpers.hh>
#include <ACGL/OpenGL/Creator/VertexArrayObjectCreator.hh>
#include <ACGL/OpenGL/Creator/ShaderProgramCreator.hh>
#include <ACGL/OpenGL/Managers.hh>
#include <ACGL/Scene/GenericCamera.hh>
#include <ACGL/Base/Settings.hh>
#include <ACGL/Utils/StringHelpers.hh>


#include <ACGL/HardwareSupport/SimpleRiftController.hh>

using namespace std;
using namespace ACGL::OpenGL;
using namespace ACGL::Base;
using namespace ACGL::Utils;
using namespace ACGL::Scene;
using namespace ACGL::Utils::StringHelpers;
using namespace ACGL::HardwareSupport;

// global variables are used to keep the example small
//
// the size of the window to open:
GLFWwindow* g_window;
glm::uvec2 g_windowSize( 1280, 800 );
bool g_distortRendering = true;

bool glfwWindowClosed = false;
SharedArrayBuffer bunny;
SharedVertexArrayObject vaoBunny;
ConstSharedShaderProgram simpleShader;
SharedTexture2D bunnyTexture;

SharedFrameBufferObject fboSideBySide;
std::vector<SharedTexture2D> offScreenTexturesSideBySide;

SharedFrameBufferObject fboTwoTextureLeft;  // for non-side-by-side rendering
SharedFrameBufferObject fboTwoTextureRight; // for non-side-by-side rendering
std::vector<SharedTexture2D> offScreenTexturesTwoTexture;

ACGL::Scene::SharedGenericCamera g_camera;

// one controller for one Rift:
SimpleRiftController *g_src;

void setOffscreenViewportSize( glm::uvec2 _size );

void setGLFWHintsForOpenGLVersion( unsigned int _version )
{
#ifdef __APPLE__
#if (ACGL_OPENGL_VERSION >= 30)
    // request OpenGL 3.2, will return a 4.1 context on Mavericks
    glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 3 );
    glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 2 );
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
#else
// non-apple
    glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, _version / 10 );
    glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, _version % 10 );
    #ifdef ACGL_OPENGL_PROFILE_CORE
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    #endif
#endif
}

/**********************************************************************************************************************
 * Returns true if a window with the desired context could get created.
 * Requested OpenGL version gets set by ACGL defines.
 */
bool createWindow()
{
    /////////////////////////////////////////////////////////////////////////////////////
    // Initialise GLFW
    //
    if ( !glfwInit() )
    {
        error() << "Failed to initialize GLFW" << endl;
        exit( -1 );
    }

    /////////////////////////////////////////////////////////////////////////////////////
    // Configure OpenGL context
    //
    setGLFWHintsForOpenGLVersion( ACGL_OPENGL_VERSION );

    /////////////////////////////////////////////////////////////////////////////////////
    // try to create an OpenGL context in a window and check the supported OpenGL version:
    //
    //                                                    R,G,B,A, Depth,Stencil
    g_window = glfwCreateWindow( g_windowSize.x, g_windowSize.y, "ACGL GLFWExamples", NULL, NULL);
    if (!g_window) {
        error() << "Failed to open a GLFW window - requested OpenGL: " <<  ACGL_OPENGL_VERSION << endl;
        return false;
    }
    glfwMakeContextCurrent(g_window);
    ACGL::init();

    return true;
}

void initResources()
{
    // define where shaders can be found:
    Settings::the()->setShaderPath("shader/");

    bunnyTexture = loadTexture2D( "../../shared/Geometry/clownfishBunny.png" );

    //
    // FBO and offscreen texture setup for side-by-side rendering
    //
    offScreenTexturesSideBySide.push_back( SharedTexture2D( new Texture2D(g_windowSize, GL_DEPTH24_STENCIL8) ) );
    offScreenTexturesSideBySide.push_back( SharedTexture2D( new Texture2D(g_windowSize) ) ); // RGBA per default
    for (size_t i = 0; i < offScreenTexturesSideBySide.size(); ++i) {
        offScreenTexturesSideBySide[i]->setMinFilter( GL_LINEAR );
        offScreenTexturesSideBySide[i]->setMagFilter( GL_LINEAR );
    }

    fboSideBySide = SharedFrameBufferObject(new FrameBufferObject());
    fboSideBySide->attachColorTexture( "oColor",  offScreenTexturesSideBySide[1] );
    fboSideBySide->setDepthTexture(               offScreenTexturesSideBySide[0] );
    fboSideBySide->validate(); // always a good idea


    //
    // FBO and offscreen texture setup for two texture rendering
    //
    offScreenTexturesTwoTexture.push_back( SharedTexture2D( new Texture2D(g_windowSize, GL_DEPTH24_STENCIL8) ) ); // RGBA per default
    offScreenTexturesTwoTexture.push_back( SharedTexture2D( new Texture2D(g_windowSize) ) ); // RGBA per default
    offScreenTexturesTwoTexture.push_back( SharedTexture2D( new Texture2D(g_windowSize) ) ); // RGBA per default
    for (size_t i = 0; i < offScreenTexturesTwoTexture.size(); ++i) {
        offScreenTexturesTwoTexture[i]->setMinFilter( GL_LINEAR );
        offScreenTexturesTwoTexture[i]->setMagFilter( GL_LINEAR );
    }

    fboTwoTextureLeft = SharedFrameBufferObject(new FrameBufferObject());
    fboTwoTextureLeft->attachColorTexture( "oColor",  offScreenTexturesTwoTexture[1] );
    fboTwoTextureLeft->setDepthTexture(               offScreenTexturesTwoTexture[0] );
    fboTwoTextureLeft->validate(); // always a good idea

    fboTwoTextureRight = SharedFrameBufferObject(new FrameBufferObject());
    fboTwoTextureRight->attachColorTexture( "oColor",  offScreenTexturesTwoTexture[2] );
    fboTwoTextureRight->setDepthTexture(               offScreenTexturesTwoTexture[0] ); // yes, share the depth buffer
    fboTwoTextureRight->validate(); // always a good idea

    // look up all shader files starting with 'HelloWorld' and build a ShaderProgram from it:
    // store it in a manager to allow semi-automatic reloading
    simpleShader = ShaderProgramFileManager::the()->get( ShaderProgramCreator("HelloWorld").fragmentDataLocations( fboSideBySide->getAttachmentLocations() ) );

    // load the geometry of the stanford bunny and build a VAO:
    vaoBunny = VertexArrayObjectCreator("../../shared/Geometry/Bunny.obj").create();

    // connect the attribute locations as they are defined in the shader to the bunny:
    vaoBunny->setAttributeLocations( simpleShader->getAttributeLocations() );

    // just in case: check for errors
    openGLCriticalError();

    setOffscreenViewportSize( g_windowSize );
}

//
// The draw function is stereo agnostic: just call it twice and set the cameras eye correctly before.
//
void draw( double time )
{
    // set view and projection matrix:
    simpleShader->use();
    simpleShader->setTexture( "uTexture", bunnyTexture, 0 );
    float s = 1.0f/8.0f;
    glm::mat4 modelMatrix = glm::scale( glm::vec3(s,s,s) );
    simpleShader->setUniform( "uModelMatrix", modelMatrix );

    // get the view from a camera that is attached to the SimpleRiftController:
    glm::mat4 viewMatrix = g_src->getCamera()->getViewMatrix();
    simpleShader->setUniform( "uViewMatrix", viewMatrix );

    // get the projection from the SimpleRiftController directly:
    simpleShader->setUniform( "uProjectionMatrix", g_src->getProjectionMatrixFromCamera() );
    simpleShader->setUniform( "uNormalMatrix", glm::inverseTranspose(glm::mat3(viewMatrix)*glm::mat3(modelMatrix)) );

    // render the bunny:
    vaoBunny->bind();
    vaoBunny->render();
    openGLCriticalError();
}

//
// Renders the scene 'side-by-side' so each eye takes up half the final rendered image.
//
void renderPassMainSideBySide( double time )
{
    fboSideBySide->bind();
    glViewport( 0,0, g_camera->getViewportWidth()*2, g_camera->getViewportHeight() );
    // clear the framebuffer:
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // left
    glViewport( 0,0, g_camera->getViewportWidth(), g_camera->getViewportHeight() );
    g_camera->setEye( GenericCamera::EYE_LEFT );
    draw( time );

    // right
    glViewport( g_camera->getViewportWidth(), 0, g_camera->getViewportWidth(), g_camera->getViewportHeight() );
    g_camera->setEye( GenericCamera::EYE_RIGHT );
    draw( time );
}

//
//
//
void renderPassMainTwoTexture( double time )
{
    // left
    fboTwoTextureLeft->bind();
    glViewport( 0,0, g_camera->getViewportWidth(), g_camera->getViewportHeight() );
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    g_camera->setEye( GenericCamera::EYE_LEFT );
    draw( time );

    // right
    fboTwoTextureRight->bind();
    glViewport( 0,0, g_camera->getViewportWidth(), g_camera->getViewportHeight() );
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    g_camera->setEye( GenericCamera::EYE_RIGHT );
    draw( time );
}


bool spaceIsPressed = false;

void handleInput( double time )
{
    static double timeOfLastFrame = 0.0;
    if (timeOfLastFrame == 0.0) {
        timeOfLastFrame = time;
        // ignore the first frame, as the user has not done useful inputs till now anyway
        return;
    }

    // make camera movements based on the elapsed time and not based on frames rendered!
    double timeElapsed = time - timeOfLastFrame;

    double speed = 1.0; // magic value to scale the camera speed

    // as long as the keys are hold down, these are triggered each frame:
    if (glfwGetKey( g_window, 'W')) { // upper case!
        g_camera->moveForward( timeElapsed*speed );
    }
    if (glfwGetKey( g_window, 'A')) { // upper case!
        g_camera->moveLeft( timeElapsed*speed );
    }
    if (glfwGetKey( g_window, 'S')) { // upper case!
        g_camera->moveBack( timeElapsed*speed );
    }
    if (glfwGetKey( g_window, 'D')) { // upper case!
        g_camera->moveRight( timeElapsed*speed );
    }
    if (glfwGetKey( g_window, '1')) {
        setOffscreenViewportSize( glm::uvec2(1280, 800) );
    }
    if (glfwGetKey( g_window, '2')) {
        glm::vec2 s = glm::vec2(1280, 800);
        s *= 1.25;
        setOffscreenViewportSize( glm::uvec2(s) );
    }
    if (glfwGetKey( g_window, '3')) {
        glm::vec2 s = glm::vec2(1280, 800);
        s *= 1.5;
        setOffscreenViewportSize( glm::uvec2(s) );
    }
    if (glfwGetKey( g_window, '4')) {
        glm::vec2 s = glm::vec2(1280, 800);
        s *= 2.0;
        setOffscreenViewportSize( glm::uvec2(s) );
    }
    if (glfwGetKey( g_window, '5')) {
        g_src->setDistortionScaleFactor( 1.0f );
    }
    if (glfwGetKey( g_window, '6')) {
        g_src->setDistortionScaleFactor( 1.25f );
    }
    if (glfwGetKey( g_window, '7')) {
        g_src->setDistortionScaleFactor( 1.5f );
    }
    if (glfwGetKey( g_window, '8')) {
        g_src->setDistortionScaleFactor( 1.75f );
    }
    if (glfwGetKey( g_window, '9')) {
        g_src->setDistortionScaleFactor( 2.0f );
    }
    g_distortRendering = true;
    if (glfwGetKey( g_window, 'X')) {
        g_distortRendering = false;
    }

    // only react on pressed and releases. There are other ways, look up the GLFW documentation
    if (!spaceIsPressed && glfwGetKey( g_window, GLFW_KEY_SPACE)) {
        // space is just pressed down
        debug() << "space pressed" << endl;
        spaceIsPressed = true;
    } else if (spaceIsPressed && !glfwGetKey( g_window, GLFW_KEY_SPACE)) {
        debug() << "space released" << endl;
        spaceIsPressed = false;
    }


    timeOfLastFrame = time;
}

void mouseMoveCallback( GLFWwindow *, double x, double y )
{
    static glm::ivec2 initialPosition;
    static bool leftMouseButtonDown = false;

    if (!leftMouseButtonDown && glfwGetMouseButton( g_window, GLFW_MOUSE_BUTTON_1 )) {
        leftMouseButtonDown = true;
        initialPosition = glm::ivec2( x,y );
        glfwSetInputMode( g_window, GLFW_CURSOR_HIDDEN, true ); // hide the cursor
        return;
    } else if (leftMouseButtonDown && !glfwGetMouseButton( g_window, GLFW_MOUSE_BUTTON_1 )) {
        leftMouseButtonDown = false;
        glfwSetInputMode( g_window, GLFW_CURSOR_HIDDEN, false ); // unhide the cursor
        glfwSetCursorPos( g_window, initialPosition.x, initialPosition.y );
        return;
    }

    glm::ivec2 movement = glm::ivec2( x,y ) - initialPosition;

    if (leftMouseButtonDown) {
        glm::vec2 realtiveMovement = glm::vec2(movement) / glm::vec2(g_windowSize);
        g_camera->FPSstyleLookAround( realtiveMovement.x, realtiveMovement.y );
        glfwSetCursorPos( g_window, initialPosition.x, initialPosition.y );
    }
}

void resizeCallback( GLFWwindow *, int newWidth, int newHeight )
{
    g_windowSize = glm::uvec2( newWidth, newHeight );
    g_src->setOutputViewportSize( g_windowSize );
}

// full viewport, one eye is half the width:
void setOffscreenViewportSize( glm::uvec2 _size )
{
    static glm::uvec2 oldSize;
    if (_size == oldSize) return;

    debug() << "set size " << toString( _size ) << endl;
    g_camera->resize( _size.x/2, _size.y );
    for (uint32_t i = 0; i < offScreenTexturesSideBySide.size(); ++i) {
        offScreenTexturesSideBySide[i]->resize( _size );
    }

    glm::uvec2 twoTextureViewport = glm::uvec2( _size.x/2, _size.y );
    for (uint32_t i = 0; i < offScreenTexturesTwoTexture.size(); ++i) {
        offScreenTexturesTwoTexture[i]->resize( twoTextureViewport );
    }
    oldSize = _size;
}


int main( int argc, char *argv[] )
{
    /////////////////////////////////////////////////////////////////////////////////////
    // Create OpenGL capable window:
    //
    if ( !createWindow() ) {
        glfwTerminate();
        exit( -1 );
    }

    g_src = new SimpleRiftController();

    g_camera = g_src->getCamera();
    g_camera->setNearClippingPlane( 0.01 );
    g_camera->setFarClippingPlane( 100.0 );
    float height = 0.25f;
    g_camera->setPosition( glm::vec3(0.0f, height, 1.5f) );
    g_camera->setTarget(   glm::vec3(0.0f, height, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f) );

    /////////////////////////////////////////////////////////////////////////////////////
    // Set window title to binary name (without the path):
    //
    std::vector<std::string> tmp = StringHelpers::split( std::string( argv[0] ), '/' );
    glfwSetWindowTitle( g_window, tmp[tmp.size()-1].c_str() );

    glfwSetWindowSizeCallback(  g_window, resizeCallback );

    // Enable vertical sync (on cards that support it) with parameter 1 - 0 means off
    glfwSwapInterval( 1 );

    /////////////////////////////////////////////////////////////////////////////////////
    // OpenGL state:
    //
    glClearColor( 0.0, 0.0, 0.0, 1.0 );
    glEnable( GL_DEPTH_TEST );


    //
    // configure input:
    //
    // don't miss short keypresses, which are shorter than one frame and would be missed otherwise:
    glfwSetInputMode( g_window, GLFW_STICKY_KEYS, true );
    //
    // handle the mouse as a callback:
    glfwSetCursorPosCallback( g_window, mouseMoveCallback );


    initResources();
    double startTimeInSeconds = glfwGetTime();


    do {
        double now = glfwGetTime() - startTimeInSeconds;

        // shader file reloading once a second:
        static double nextReloadTime = 1.0;
        if (now > nextReloadTime) {
            ShaderProgramFileManager::the()->updateAll();
            nextReloadTime = now + 1.0; // check again in one second
        }

        // input
        glfwPollEvents();
        handleInput( now );
        g_src->updateCamera();

        // rendering
        if (spaceIsPressed) {
            renderPassMainSideBySide( now );
            g_src->renderDistorted( offScreenTexturesSideBySide[1] );
        } else {
        // alternative rendering:
            renderPassMainTwoTexture( now );
            g_src->renderDistorted( offScreenTexturesTwoTexture[1], offScreenTexturesTwoTexture[2] );
        }

        glfwSwapBuffers( g_window );
        openGLCriticalError();
    } while( !glfwWindowShouldClose( g_window ) );

    delete g_src;
    glfwTerminate();
    exit(0);
}

